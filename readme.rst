Phurlite
Super Simple URL Shortener
PHP + URL + SQLite = Phurlite
2012 - Dustin Davis
http://www.nerdydork.com
License: Public Domain
=============================

Goals of this script:
 * Simple
 * 1 Self-contained PHP file + .htaccess file
 * SQLite for storage
 * API for use with Tweetbot

See blog post: http://www.nerdydork.com/phurlite-simple-php-sqlite-url-shortener.html